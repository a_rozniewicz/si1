package arozniewicz.graphcoloring;

import arozniewicz.graphcoloring.geneticalgorithm.GeneticAlgorithm;

public class Main {

	public static void main(String[] args) {
		GeneticAlgorithm geneticAlgorithm = new GeneticAlgorithm("graphs/queen14_14.col", 150, 60000, 0.65f, 0.005f);
		geneticAlgorithm.compute((byte) 16);

//		LargestFirst largestFirst = new LargestFirst("graphs/queen14_14.col");
//		largestFirst.compute();
//		System.out.println(largestFirst.getSmallestValue());
	}
}
