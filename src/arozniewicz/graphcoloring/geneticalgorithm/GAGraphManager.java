package arozniewicz.graphcoloring.geneticalgorithm;

import java.util.Random;

import arozniewicz.graphcoloring.model.Graph;

public class GAGraphManager {
	private static final Random RANDOM = new Random(System.currentTimeMillis());
	
	private GAGraphManager() {
		
	}
	 
	public static void setRandomGraphValues(Graph graph, byte maxValue) {
		for (int i = 0; i < graph.getVertexCount(); ++i)
			graph.setVertexValue(i, (byte) RANDOM.nextInt(maxValue));
	}
	
	public static void mutateGraph(Graph graph, float probabilty, byte maxValue) {
		int vertexCount = graph.getVertexCount();
		
		for (int i = 0; i < vertexCount; ++i) {
			if (RANDOM.nextFloat() <= probabilty)
				graph.setVertexValue(i, (byte) RANDOM.nextInt(maxValue));
		}
	}
	
	public static void crossGraphs(Graph graphA, Graph graphB) {
		int vertexCount = graphA.getVertexCount();

		int a = RANDOM.nextInt(vertexCount);
		int b = RANDOM.nextInt(vertexCount);
		if (a > b) {
			int temp = a;
			a = b;
			b = temp;
		}
		
		byte tempValue;
		for (int i = a; i < b; ++i) {
			tempValue = graphA.getVertexValue(i);
			graphA.setVertexValue(i, graphB.getVertexValue(i));
			graphB.setVertexValue(i, tempValue);
		}
	}
}