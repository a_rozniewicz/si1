package arozniewicz.graphcoloring.geneticalgorithm;

import java.io.IOException;
import java.util.Random;

import arozniewicz.graphcoloring.model.AdjacencyList;
import arozniewicz.graphcoloring.model.Graph;
import arozniewicz.graphcoloring.print.FilePrinter;

public class GeneticAlgorithm {
	private static final Random RANDOM = new Random(System.currentTimeMillis());
	private static final int TOURNAMENT_SIZE = 5;

	private final AdjacencyList adjacencyList;

	private float crossoverProbability, mutationProbability;
	private float minFitnessValue, maxFitnessValue, avgFitnessValue;
	private int populationCount, maxGenerationsCount;

	public GeneticAlgorithm(String fileName, int populationCount, int maxGenerationsCount, float crossoverProbability, float mutationProbability) {
		this.adjacencyList = new AdjacencyList(fileName);

		this.crossoverProbability = crossoverProbability;
		this.mutationProbability = mutationProbability;
		this.populationCount = populationCount;
		this.maxGenerationsCount = maxGenerationsCount;
	}

	public Graph compute(byte maxValue) {
		int t = 1;

		Graph[] population = initialise(maxValue);
		evaluate(population);

		FilePrinter filePrinter = new FilePrinter("data.txt");
		try {
			filePrinter.open();

			while (minFitnessValue > 0 && t <= maxGenerationsCount) {
				select(population);
				cross(population);
				mutate(population, maxValue);

				evaluate(population);

				System.out.println(t + " " + minFitnessValue + " " + maxFitnessValue + " " + avgFitnessValue);
				filePrinter.write(t, minFitnessValue, maxFitnessValue, avgFitnessValue);

				++t;
			}

			filePrinter.close();
		} catch (IOException e) {

		}

		for (int i = 0; i < population.length; ++i) {
			if (population[i].getFitnessValue() == 0.0f)
				return population[i];
		}

		return null;
	}

	private Graph[] initialise(byte maxValue) {
		Graph[] population = new Graph[populationCount];

		for (int i = 0; i < populationCount; ++i) {
			population[i] = new Graph(adjacencyList.getVerticesCount());
			GAGraphManager.setRandomGraphValues(population[i], maxValue);
		}

		return population;
	}

	private void evaluate(Graph[] population) {
		minFitnessValue = Integer.MAX_VALUE;
		maxFitnessValue = Integer.MIN_VALUE;

		avgFitnessValue = 0.0f;

		for (Graph graph : population) {
			graph.calculateGraphValue(adjacencyList);
			avgFitnessValue += graph.getFitnessValue();

			if (graph.getFitnessValue() < minFitnessValue) {
				minFitnessValue = graph.getFitnessValue();
			}

			if (graph.getFitnessValue() > maxFitnessValue) {
				maxFitnessValue = graph.getFitnessValue();
			}
		}

		avgFitnessValue /= populationCount;
	}

	private void select(Graph[] population) {
		Graph graph, bestGraph = null;
		float minValue = Integer.MAX_VALUE;

		for (int i = 0; i < populationCount; ++i) {
			for (int j = 0; j < TOURNAMENT_SIZE; ++j) {
				graph = population[RANDOM.nextInt(populationCount)];
				
				if (graph.getFitnessValue() < minValue) {
					minValue = graph.getFitnessValue();
					bestGraph = graph;
				}
			}
			
			population[i] = new Graph(bestGraph);
		}
	}

	private void cross(Graph[] population) {
		Graph graphA, graphB, tempGraph;
		int graphBIndex;

		for (int i = 0; i < populationCount - 1; ++i) {
			if (RANDOM.nextFloat() <= crossoverProbability) {
				graphA = population[i];
				graphB = population[(graphBIndex = i + 1 + RANDOM.nextInt(populationCount - i - 1))];
				GAGraphManager.crossGraphs(graphA, graphB);

				tempGraph = population[i + 1];
				population[i + 1] = graphB;
				population[graphBIndex] = tempGraph;

				++i;
			}
		}
	}

	private Graph[] mutate(Graph[] population, byte maxValue) {
		for (Graph graph : population) {
			GAGraphManager.mutateGraph(graph, mutationProbability, maxValue);
		}

		return population;
	}
}
