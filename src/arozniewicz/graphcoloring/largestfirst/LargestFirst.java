package arozniewicz.graphcoloring.largestfirst;

import arozniewicz.graphcoloring.model.AdjacencyList;
import arozniewicz.graphcoloring.model.Graph;

public class LargestFirst {
	private final AdjacencyList adjacencyList;
	
	private byte smallestValue = 0;
	
	public LargestFirst(String fileName) {
		this.adjacencyList = new AdjacencyList(fileName);
	}
	
	public LargestFirst(AdjacencyList adjacencyList) {
		this.adjacencyList = adjacencyList;
	}
	
	public Graph compute() {
		this.smallestValue = 0;
		
		int verticesCount = adjacencyList.getVerticesCount();
		Graph graph = new Graph(verticesCount);
		
		int maxAdjacentVerticesCount = 0;
		for (int i = 0; i < verticesCount; ++i) {
			if (adjacencyList.getAdjacentVertices(i).length > maxAdjacentVerticesCount)
				maxAdjacentVerticesCount = adjacencyList.getAdjacentVertices(i).length;
		}
		
		int filledVertexCount = 0;
		
		while (filledVertexCount < verticesCount) {
			for (int i = 0; i < verticesCount; ++i) {
				if (adjacencyList.getAdjacentVertices(i).length == maxAdjacentVerticesCount) {
					fillVertex(graph, i);
					++filledVertexCount;
				}
			}
			
			--maxAdjacentVerticesCount;
		}
		
		return graph;
	}
	
	private byte fillVertex(Graph graph, int vertexNumber) {
		byte smallestValue = 0;
		boolean conflict;
		
		do {
			conflict = false;
			
			for (int i = 0; i <= vertexNumber; ++i) {
				for (int vertex: adjacencyList.getAdjacentVertices(i)) {
					if (i != vertexNumber && vertex == vertexNumber && graph.getVertexValue(i) == smallestValue) {
						++smallestValue;
						conflict = true;
						
						break;
					} else if (i == vertexNumber && graph.getVertexValue(vertex) == smallestValue) {
						++smallestValue;
						conflict = true;
						
						break;
					}
				}
			}
		} while (conflict);
		
		graph.setVertexValue(vertexNumber, smallestValue);
		if (smallestValue > this.smallestValue)
			this.smallestValue = smallestValue;
		
		return smallestValue;
	}

	public byte getSmallestValue() {
		return smallestValue;
	}
}