package arozniewicz.graphcoloring.model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AdjacencyList {
	private short[][] adjacencyList;

	public AdjacencyList(int verticesCount) {
		this.adjacencyList = new short[verticesCount][];
	}
	
	public AdjacencyList(String fileName) {
		try {
			if (fileName.substring(fileName.length() - 2, fileName.length()).equals(".b")) {
				this.adjacencyList = createAdjacencyListFromBinaryFile(fileName);
			} else
				this.adjacencyList = createAdjacencyListFromFile(fileName);
		} catch (FileNotFoundException e) {
			System.out.println("Adjacency list file not found.");
		} catch (IOException e) {
			System.out.println("In-out exception while reading adjacency list file.");
		}
	}
	
	public int getVerticesCount() {
		return adjacencyList == null ? 0 : adjacencyList.length;
	}
		
	public void setAdjacentVertices(int vertexNumber, short... adjacentVertices) {
		this.adjacencyList[vertexNumber] = adjacentVertices;
	}
	
	public short[] getAdjacentVertices(int vertexNumber) {
		return adjacencyList[vertexNumber];
	}
	
	public String toString() {
		if (adjacencyList == null) {
			return "";
		} else {
			StringBuilder stringBuilder = new StringBuilder();
			
			int i = 0;
			for (short[] adjacentVertices: adjacencyList) {
				stringBuilder.append(i++).append(": ");
				
				for (short vertex: adjacentVertices) 
					stringBuilder.append(vertex).append(" ");
				
				stringBuilder.append("\n");
			}
				
			return stringBuilder.toString();
		}
	}
	
	private static short[][] createAdjacencyListFromBinaryFile(String fileName) {
		System.out.println("Binary files not supported yet.");
		
		return null;
	}
	
	private static short[][] createAdjacencyListFromFile(String fileName) throws FileNotFoundException, IOException {
		FileReader fileReader = new FileReader(fileName);
		BufferedReader bufferedReader = new BufferedReader(fileReader);

		try {
			String line;

			do {
				line = bufferedReader.readLine();
			} while (line.charAt(0) != 'p');

			String[] arguments = line.split(" ");

			short[][] result = new short[Integer.parseInt(arguments[2])][];
			for (int i = 0; i < result.length; ++i)
				result[i] = new short[] {};
			
			List<Short> adjacentVertices = new ArrayList<Short>();
			short vertexNumber = -1;

			line = bufferedReader.readLine();
			if (line != null && !line.equals("")) {
				arguments = line.split(" ");
				vertexNumber = (short)(Short.parseShort(arguments[1]));
			}
			
			do {
				arguments = line.split(" ");
				
				if (vertexNumber != Short.parseShort(arguments[1])) {
					result[vertexNumber - 1] = new short[adjacentVertices.size()];
					
					for (int i = 0; i < adjacentVertices.size(); ++i)
						result[vertexNumber - 1][i] = (short)(adjacentVertices.get(i) - 1);
					
					adjacentVertices.clear();
					
					vertexNumber = Short.parseShort(arguments[1]);
				}
				
				adjacentVertices.add(Short.parseShort(arguments[2]));

				line = bufferedReader.readLine();
			} while (line != null && !line.equals(""));

			result[vertexNumber - 1] = new short[adjacentVertices.size()];
			
			for (int i = 0; i < adjacentVertices.size(); ++i)
				result[vertexNumber - 1][i] = (short)(adjacentVertices.get(i) - 1);
			
			return result;
		} finally {
			if (bufferedReader != null)
				bufferedReader.close();
		}
	}
	
	/*
	public static void createAdjacencyMatrix(String fileName) {
		if (fileName.substring(fileName.length() - 2, fileName.length()).equals(".b")) {
			matrix = decodeBinaryFile(fileName);
		} else
			matrix = readFile(fileName);
	}

	@SuppressWarnings("deprecation")
	private static short[][] decodeBinaryFile(String fileName) {
		short[][] result;

		try {
			FileInputStream fileInputStream = new FileInputStream(fileName);
			DataInputStream dataInputStream = new DataInputStream(fileInputStream);

			String string;

			do {
				string = dataInputStream.readLine();
			} while (string.charAt(0) != 'p');

			String[] arguments = string.split(" ");

			result = new short[Integer.parseInt(arguments[2])][];

			byte[] bytes;
			for (int i = 0; i < result.length; ++i) {
				result[i] = new short[(i + 8) / 8];

				bytes = new byte[(i + 8) / 8];
				dataInputStream.read(bytes);

				for (int j = 0; j < bytes.length; ++j)
					result[i][j] = (short) (bytes[j] & 0xff);
			}

			fileInputStream.close();
		} catch (IOException e) {
			return null;
		}

		return result;
	}

	private static short[][] readFile(String fileName) {
		short[][] result;

		try {
			FileReader fileReader = new FileReader(fileName);
			BufferedReader bufferedReader = new BufferedReader(fileReader);

			String string;

			do {
				string = bufferedReader.readLine();
			} while (string.charAt(0) != 'p');

			String[] arguments = string.split(" ");

			result = new short[Integer.parseInt(arguments[2])][];

			for (int i = 0; i < result.length; ++i)
				result[i] = new short[(i + 8) / 8];

			int i, j;
			while ((string = bufferedReader.readLine()) != null && !string.equals("")) {
				arguments = string.split(" ");

				i = Integer.parseInt(arguments[2]) - 1;
				j = Integer.parseInt(arguments[1]) - 1;

				result[i][j / 8] = (short) (result[i][j / 8] | (1 << (7 - (j - (j / 8) * 8))));
			}

			bufferedReader.close();
		} catch (IOException e) {
			return null;
		}

		return result;
	}
	*/
}
