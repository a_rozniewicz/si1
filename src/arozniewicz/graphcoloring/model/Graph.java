package arozniewicz.graphcoloring.model;

public class Graph {
	private final int vertexCount;
	private byte[] vertexValues;
	
	private float fitnessValue;

	public Graph(int vertexCount) {
		this.vertexCount = vertexCount;
		
		this.vertexValues = new byte[vertexCount];
		
		for (int i = 0; i < vertexCount; ++i)
			setVertexValue(i, (byte) -1);
	}
	
	public Graph(Graph graph) {
		this.vertexCount = graph.getVertexCount();

		this.vertexValues = new byte[vertexCount];
		
		for (int i = 0; i < vertexCount; ++i)
			setVertexValue(i, graph.getVertexValue(i));
	}
	
	public int calculateGraphValue(AdjacencyList adjacencyList) {
		int value = 0;
		
		for (int i = 0; i < adjacencyList.getVerticesCount(); ++i) {
			for (short adjacentVertex: adjacencyList.getAdjacentVertices(i)) {
				if (this.getVertexValue(i) == this.getVertexValue(adjacentVertex))
					++value;
			}
		}

		setFitnessValue(value);
		
		return value;
	}
	
	public int getVertexCount() {
		return vertexCount;
	}
	
	public float getFitnessValue() {
		return fitnessValue;
	}

	public void setFitnessValue(float value) {
		this.fitnessValue = value;
	}

	public byte getVertexValue(int vertexNumber) {
		return vertexValues[vertexNumber];
	}
	
	public void setVertexValue(int vertexNumber, byte vertexValue) {
		vertexValues[vertexNumber] = vertexValue;
	}
	
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		
		for (int i = 0; i < vertexCount; ++i)
			stringBuilder.append(i).append(": ").append(vertexValues[i]).append(" | ");
		
		return stringBuilder.toString();
	}
}