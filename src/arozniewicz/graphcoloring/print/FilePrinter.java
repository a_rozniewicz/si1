package arozniewicz.graphcoloring.print;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class FilePrinter {
	private final String fileName;
	
	private FileWriter fileWriter;
	private BufferedWriter bufferedWriter;
	
	private StringBuilder stringBuilder = new StringBuilder();
	
	public FilePrinter(String fileName) {
		this.fileName = fileName;
	}
	
	public void open() throws IOException {
		fileWriter = new FileWriter(fileName, false);
		bufferedWriter = new BufferedWriter(fileWriter);
	}
	
	public void write(float... data) throws IOException {
		for (float f: data)
			stringBuilder.append(f).append("; ");
		
		stringBuilder.append("\n");
		
		bufferedWriter.write(stringBuilder.toString());
		bufferedWriter.flush();
		
		stringBuilder.setLength(0);
	}
	
	public void close() throws IOException {
		bufferedWriter.close();
	}
}
